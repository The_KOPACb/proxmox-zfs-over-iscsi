# Proxmox ZFS over ISCSI ansible role
for https://github.com/TheGrandWazoo/freenas-proxmox plugin

I`m using it with TrueNas Scale and didnt test with TrueNas Core, but seems like it should work.

## Inventory:
```yaml
proxmox:
  hosts:
    192.168.88.10:
    192.168.88.11:
  vars:
   truenas_api: 192.168.89.244
   pve_no_subscription_repo: true
   ceph_repo_disable: true


truenas:
  hosts:
    192.168.89.244:
```
`192.168.88.x` - regular network  
`192.168.89.x` - SAN network, if exists. can be same as the regular network.


`proxmox hosts`  - IP of proxmox hosts  
`truenas hosts` - ip of truenas, in SAN network  
`proxmox vars truenas_api` - ip of truenas API for proxmox's. can be in SAN network.  
`proxmox vars pve_no_subscription_repo` switch pve-enterprise repo to pve-no-subscription if `true`  
`proxmox vars ceph_repo_disable` disable ceph repo if `true`  

## Info

ZFS-over-ISCSI plugin uses both API and ssh connection to Truenas.  
Playbook will generate ssh keys for Proxmox and will add pubkey to Truenas.  
Keys stored in `ssh_keys_truenas` directory.


## Usage

* copy `inventory.yml.sample` to `inventory.yml`  
	```bash
	cp inventory.yml.sample inventory.yml
	```

* edit `inventory.yml`  
* run
	* make shure that your ssh keys present on all hosts ( root@proxmoxes and root@truenas )
	* If you dont have ansible locally:  
	 ```bash
	 docker run -it --rm -v ~/.ssh:/tmp/.ssh:ro -v $(pwd):/ansible/ willhallonline/ansible:2.16.4-bookworm bash
	 ```
	 ```
	cp -r /tmp/.ssh/ ~/
	 ```

	 * run playbook
	 ```bash
	 ansible-playbook -i inventory.yml main.yml
	 ```

# Complete Guide
https://xinux.net/index.php/Proxmox_iscsi_over_zfs_with_freenas


# TrueNAS ZFS over iSCSI Plugin

https://github.com/TheGrandWazoo/freenas-proxmox


